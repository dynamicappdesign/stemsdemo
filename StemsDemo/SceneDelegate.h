//
//  SceneDelegate.h
//  StemsDemo
//
//  Created by Peter Robert on 04/08/2020.
//  Copyright © 2020 Peter Robert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

