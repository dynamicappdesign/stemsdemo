//
//  ViewController.m
//  StemsDemo
//
//  Created by Peter Robert on 04/08/2020.
//  Copyright © 2020 Peter Robert. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreML/CoreML.h>

#import "tasnet_leaky.h"
//#import <Firebase.h>
//#import <FirebaseMLCommon/FirebaseMLCommon.h>
//#import <TensorFlowLiteC/TensorFlowLiteC.h>
#import <TFLTensorFlowLite/TFLTensorFlowLite.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    BOOL useTensorFlowLite = YES;
    
    if (useTensorFlowLite) {
        
        NSError *tfLiteError = nil;
        TFLInterpreter *interpreter = [[TFLInterpreter alloc] initWithModelPath:[[NSBundle mainBundle] pathForResource:@"tasnet" ofType:@"tflite"] error:&tfLiteError];
        if (tfLiteError != nil) {
            NSLog(@"Error: %@",tfLiteError);
        }
        
        [interpreter allocateTensorsWithError:&tfLiteError];
        if (tfLiteError != nil) {
            NSLog(@"Error: %@",tfLiteError);
        }
        
        NSLog(@"Input tensor count: %lu",(unsigned long)interpreter.inputTensorCount);
        NSLog(@"Output tensor count: %lu",(unsigned long)interpreter.outputTensorCount);
        
        TFLTensor *inputTensor = [interpreter inputTensorAtIndex:0 error:&tfLiteError];
        if (tfLiteError != nil) {
            NSLog(@"Error: %@",tfLiteError);
        }
        NSLog(@"Input tensor shape: %@",[inputTensor shapeWithError:nil]);
        
        TFLTensor *outputTensor = [interpreter outputTensorAtIndex:0 error:&tfLiteError];
        if (tfLiteError != nil) {
            NSLog(@"Error: %@",tfLiteError);
        }
        NSLog(@"Output tensor shape: %@",[outputTensor shapeWithError:nil]);
        
//        TfLiteInterpreterOptions *interpreterOptions = TfLiteInterpreterOptionsCreate();
//
//        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tasnet" ofType:@"tflite"]];
//        TfLiteModel *model = TfLiteModelCreate(data.bytes, data.length);
//        TfLiteInterpreter *interpreter = TfLiteInterpreterCreate(model, interpreterOptions);
//
//
//
//        TfLiteInterpreterAllocateTensors(interpreter);
//
//        int32_t inputCount = TfLiteInterpreterGetInputTensorCount(interpreter);
//        int32_t outputCount = TfLiteInterpreterGetOutputTensorCount(interpreter);
//        NSLog(@"TFLite file has %d inputs and %d outputs",inputCount, outputCount);
//
//        TfLiteTensor *inputTensor = TfLiteInterpreterGetInputTensor(interpreter, 0);
//        TfLiteType inputType = TfLiteTensorType(inputTensor);
//        NSString *inputName = [NSString stringWithUTF8String:TfLiteTensorName(inputTensor)];
//        size_t inputTensorByteSize = TfLiteTensorByteSize(inputTensor);
//        size_t inputTensorFloat32Size = inputTensorByteSize / sizeof(Float32);
//        NSLog(@"Input tensor (%@) Float32 size: %lu", inputName,inputTensorFloat32Size);
//        int32_t inputTensorNumberOfDimensions = TfLiteTensorNumDims(inputTensor);
//        for (int i=0;i<inputTensorNumberOfDimensions;i++) {
//            int32_t dimension = TfLiteTensorDim(inputTensor, i);
//            NSLog(@"Input dimension %d: %d",i, dimension);
//        }
//
//
//        TfLiteTensor *outputTensor = TfLiteInterpreterGetOutputTensor(interpreter, 0);
//        TfLiteType outputType = TfLiteTensorType(outputTensor);
//        NSString *outputName = [NSString stringWithUTF8String:TfLiteTensorName(outputTensor)];
//        size_t outputTensorByteSize = TfLiteTensorByteSize(outputTensor);
//        size_t outputTensorFloat32Size = outputTensorByteSize / sizeof(Float32);
//        NSLog(@"Output tensor (%@) Float32 size: %lu",outputName, outputTensorFloat32Size);
//        int32_t outputTensorNumberOfDimensions = TfLiteTensorNumDims(outputTensor);
//        for (int i=0;i<outputTensorNumberOfDimensions;i++) {
//            int32_t dimension = TfLiteTensorDim(outputTensor, i);
//            NSLog(@"Output dimension %d: %d",i, dimension);
//        }
                
        //Go trough the audio file
        NSURL *audioURL = [[NSBundle mainBundle] URLForResource:@"TestShort" withExtension:@"mp3"];
        NSString *stem1Path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"stem1.caf"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:stem1Path]) {
            [[NSFileManager defaultManager] removeItemAtPath:stem1Path error:nil];
        }
        NSURL *stem1AudioUrl = [NSURL fileURLWithPath:stem1Path];
        NSString *stem2Path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"stem2.caf"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:stem1Path]) {
            [[NSFileManager defaultManager] removeItemAtPath:stem1Path error:nil];
        }
        NSURL *stem2AudioUrl = [NSURL fileURLWithPath:stem2Path];

        AVURLAsset *asset = [AVURLAsset assetWithURL:audioURL];

        //Create the audio reader
        AVAssetReader *assetReader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
        NSDictionary *readerOutputSettings = @{
                                               AVFormatIDKey:[NSNumber numberWithInt:kAudioFormatLinearPCM],
                                               AVSampleRateKey:[NSNumber numberWithDouble:8000.0],
                                               AVLinearPCMBitDepthKey:[NSNumber numberWithInt:sizeof(Float32) * 8],
                                               AVLinearPCMIsFloatKey:[NSNumber numberWithBool:YES],
                                               AVLinearPCMIsBigEndianKey:[NSNumber numberWithBool:NO],
                                               AVLinearPCMIsNonInterleaved:[NSNumber numberWithBool:NO],
                                               AVNumberOfChannelsKey:[NSNumber numberWithInt:1]
                                               };

        AVAssetReaderAudioMixOutput *readerMixOutput = [[AVAssetReaderAudioMixOutput alloc] initWithAudioTracks:[asset tracksWithMediaType:AVMediaTypeAudio] audioSettings:readerOutputSettings];
        [assetReader addOutput:readerMixOutput];
        [assetReader startReading];


        //Create the audio writer for stem1 to test
        AVAssetWriter *assetWriter = [[AVAssetWriter alloc] initWithURL:stem1AudioUrl fileType:AVFileTypeCoreAudioFormat error:&tfLiteError];
        
        if (tfLiteError != nil) {
            NSLog(@"Error: %@",tfLiteError);
        }

        AVAssetWriterInput *assetWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:readerOutputSettings];
        [assetWriter addInput:assetWriterInput];
        [assetWriter startWriting];
        [assetWriter startSessionAtSourceTime:kCMTimeZero];

        CMSampleBufferRef readSampleBufferRef;
        CMSampleBufferRef writeSampleBufferRef;
        while ((readSampleBufferRef = [readerMixOutput copyNextSampleBuffer])) {
            
            //Create a sample buffer to write to
            CMSampleBufferCreateCopy(kCFAllocatorDefault, readSampleBufferRef, &writeSampleBufferRef);
            CMBlockBufferRef writeBlockBufferRef = CMSampleBufferGetDataBuffer(writeSampleBufferRef);
            
            CMBlockBufferRef readBlockBufferRef = CMSampleBufferGetDataBuffer(readSampleBufferRef);
            size_t dataLength = CMBlockBufferGetDataLength(readBlockBufferRef);
            u_long numberOfFloat32Numbers = dataLength / sizeof(Float32);
            
            u_long numberOfBatches = numberOfFloat32Numbers / 16;
            
            for (int i=0;i<numberOfBatches;i++) {
                size_t readWriteOffset = sizeof(Float32) * i * 16;
                
                char *readDataPointer;
                CMBlockBufferGetDataPointer(readBlockBufferRef, readWriteOffset, nil, nil, &readDataPointer);
                
                //Copy to input tensor
                NSMutableData *inputData = [NSMutableData dataWithBytes:readDataPointer length:16 * sizeof(Float32)];
                
                [inputTensor copyData:inputData error:&tfLiteError];
                if (tfLiteError != nil) {
                    NSLog(@"Error: %@",tfLiteError);
                }
                
                //Process
                [interpreter invokeWithError:&tfLiteError];
                if (tfLiteError != nil) {
                    NSLog(@"Error: %@",tfLiteError);
                }
                
                NSData *data = [outputTensor dataWithError:&tfLiteError];
                if (tfLiteError != nil) {
                    NSLog(@"Error: %@",tfLiteError);
                }
                
                char *writeDataPointer;
                CMBlockBufferGetDataPointer(writeBlockBufferRef, readWriteOffset, nil, nil, &writeDataPointer);
                
                memcpy(writeDataPointer, data.bytes, 16 * sizeof(Float32));
            }

            //Writing to
            while (assetWriterInput.isReadyForMoreMediaData == NO) {

            }
            [assetWriterInput appendSampleBuffer:writeSampleBufferRef];

            //Release the sample buffers
            CFRelease(readSampleBufferRef);
            CFRelease(writeSampleBufferRef);
        }
        [assetWriter endSessionAtSourceTime:kCMTimeZero];
        [assetWriter finishWritingWithCompletionHandler:^{
            NSLog(@"File finished writing");
        }];
        
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[stem1AudioUrl] applicationActivities:nil];
        [self presentViewController:activityVC animated:YES completion:nil];
        
    } else {
        if (@available(iOS 13.0, *)) {
                
                NSError *coreMLError = nil;
                
                tasnet_leaky *model = [[tasnet_leaky alloc] init];
                MLModelDescription *description = model.model.modelDescription;
                [description.inputDescriptionsByName enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, MLFeatureDescription * _Nonnull obj, BOOL * _Nonnull stop) {
                    NSLog(@"Feature: %@, Description: %@", key, obj.multiArrayConstraint);
                }];
                
                //Go trough the audio file
                NSURL *audioURL = [[NSBundle mainBundle] URLForResource:@"TestShort" withExtension:@"mp3"];
                NSString *stem1Path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"stem1.caf"];
                if ([[NSFileManager defaultManager] fileExistsAtPath:stem1Path]) {
                    [[NSFileManager defaultManager] removeItemAtPath:stem1Path error:nil];
                }
                NSURL *stem1AudioUrl = [NSURL fileURLWithPath:stem1Path];
                NSString *stem2Path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"stem2.caf"];
                if ([[NSFileManager defaultManager] fileExistsAtPath:stem1Path]) {
                    [[NSFileManager defaultManager] removeItemAtPath:stem1Path error:nil];
                }
                NSURL *stem2AudioUrl = [NSURL fileURLWithPath:stem2Path];

                AVURLAsset *asset = [AVURLAsset assetWithURL:audioURL];

                //Create the audio reader
                AVAssetReader *assetReader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
                NSDictionary *readerOutputSettings = @{
                                                       AVFormatIDKey:[NSNumber numberWithInt:kAudioFormatLinearPCM],
                                                       AVSampleRateKey:[NSNumber numberWithDouble:44100.0],
                                                       AVLinearPCMBitDepthKey:[NSNumber numberWithInt:sizeof(Float32) * 8],
                                                       AVLinearPCMIsFloatKey:[NSNumber numberWithBool:YES],
                                                       AVLinearPCMIsBigEndianKey:[NSNumber numberWithBool:NO],
                                                       AVLinearPCMIsNonInterleaved:[NSNumber numberWithBool:NO],
                                                       AVNumberOfChannelsKey:[NSNumber numberWithInt:2]
                                                       };

                AVAssetReaderAudioMixOutput *readerMixOutput = [[AVAssetReaderAudioMixOutput alloc] initWithAudioTracks:[asset tracksWithMediaType:AVMediaTypeAudio] audioSettings:readerOutputSettings];
                [assetReader addOutput:readerMixOutput];
                [assetReader startReading];


                //Create the audio writer for stem1 to test
                AVAssetWriter *assetWriter = [[AVAssetWriter alloc] initWithURL:stem1AudioUrl fileType:AVFileTypeCoreAudioFormat error:&coreMLError];

                AVAssetWriterInput *assetWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:readerOutputSettings];
                [assetWriter addInput:assetWriterInput];
                [assetWriter startWriting];
                [assetWriter startSessionAtSourceTime:kCMTimeZero];

                CMSampleBufferRef readSampleBufferRef;
                CMSampleBufferRef writeSampleBufferRef;
                while ((readSampleBufferRef = [readerMixOutput copyNextSampleBuffer])) {
                    
                    //Create a sample buffer to write to
                    CMSampleBufferCreateCopy(kCFAllocatorDefault, readSampleBufferRef, &writeSampleBufferRef);
                    CMBlockBufferRef writeBlockBufferRef = CMSampleBufferGetDataBuffer(writeSampleBufferRef);
                    
                    CMBlockBufferRef readBlockBufferRef = CMSampleBufferGetDataBuffer(readSampleBufferRef);
                    size_t dataLength = CMBlockBufferGetDataLength(readBlockBufferRef);
                    u_long numberOfFloat32Numbers = dataLength / sizeof(Float32);
                    u_long numberOf16NumberBatches = numberOfFloat32Numbers / 16;
                    
                    
                    NSMutableArray *predictionInputs = [NSMutableArray array];
                    
                    for (int i=0;i<numberOf16NumberBatches;i++) {
                        char *readDataPointer;
                        CMBlockBufferGetDataPointer(readBlockBufferRef, sizeof(Float32) * i * 16, nil, nil, &readDataPointer);

                        MLMultiArray *multiArray = [[MLMultiArray alloc] initWithDataPointer:readDataPointer shape:@[@1, @16] dataType:MLMultiArrayDataTypeFloat32 strides:@[@1, @1] deallocator:^(void * _Nonnull bytes) {
                            //free(bytes);
                        } error:&coreMLError];

                        if (coreMLError != nil) {
                            NSLog(@"Error: %@",coreMLError);
                        }
                        
                        tasnet_leakyInput *input = [[tasnet_leakyInput alloc] initWithEncoder:multiArray];
                        [predictionInputs addObject:input];

        //                tasnet_leakyOutput *prediction = [model predictionFromEncoder:multiArray error:&coreMLError];
        //
        //                if (coreMLError != nil) {
        //                    NSLog(@"Error: %@",coreMLError);
        //                } else {
        //                    MLMultiArray *stem1Prediction = prediction.decoder;
        //                    MLMultiArray *stem2Prediction = prediction._1053;
        //
        //                    //Test1: Take the first 16 floats from the prediction and write them to the write buffer, which will be used to write to a file
        //                    char *writeDataPointer;
        //                    CMBlockBufferGetDataPointer(writeBlockBufferRef, sizeof(Float32) * i * 16, nil, nil, &writeDataPointer);
        //                    memcpy(writeDataPointer, stem1Prediction.dataPointer, sizeof(Float32) * 16);
        //                }
                    }
                    
                    //Now that the inputs are ready, make the prediction
                    MLPredictionOptions *predictionOptions = [[MLPredictionOptions alloc] init];
                    NSArray *predictions = [model predictionsFromInputs:predictionInputs options:predictionOptions error:&coreMLError];
                    
                    if (coreMLError != nil) {
                        NSLog(@"Error: %@",coreMLError);
                    } else {
                        //Test: Take the first Nx16 floats from the prediction and write them to the write buffer, which will be used to write to a file
                        char *writeDataPointer;
                        CMBlockBufferGetDataPointer(writeBlockBufferRef, 0, nil, nil, &writeDataPointer);
                        memcpy(writeDataPointer, ((tasnet_leakyOutput*)predictions[0]).decoder.dataPointer, sizeof(Float32) * 16 * predictionInputs.count);
                    }

                    //Writing to
                    while (assetWriterInput.isReadyForMoreMediaData == NO) {

                    }
                    [assetWriterInput appendSampleBuffer:writeSampleBufferRef];

                    //Release the sample buffers
                    CFRelease(readSampleBufferRef);
                    CFRelease(writeSampleBufferRef);
                }
                [assetWriter endSessionAtSourceTime:kCMTimeZero];
                [assetWriter finishWritingWithCompletionHandler:^{
                    NSLog(@"File finished writing");
                }];
                
                
                UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[stem1AudioUrl] applicationActivities:nil];
                [self presentViewController:activityVC animated:YES completion:nil];
            }
    }
    
}


@end
